//Andrew Baumher
public enum BlockColor
{
    RED, ORANGE, YELLOW, GREEN, BLUE, CYAN, PURPLE
}
