//Author: Andrew Baumher
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Sidebar extends JPanel
{

    public static NextBlockClone clone;
    public static int offset;
    public static boolean[][] nextBlock;
    public static Color color;
    public static final int INTERVAL = 35;
    private static long score;

    public Sidebar()
    {
        clone = new NextBlockClone();
        nextBlock = clone.getBlock();
        color = clone.getColor();
        score = clone.getScore();
        Timer timer = new Timer(INTERVAL, new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                tick();
            }
        });
        timer.start();

    }

    void tick()
    {
        clone = new NextBlockClone();
        nextBlock = clone.getBlock();
        color = clone.getColor();
        score = clone.getScore();
        repaint();
    }

    public void drawNext(Graphics g, int posX, int posY, boolean[][] block, Color thisColor)
    {
        for (int x = 0; x < block.length; x++)
        {
            for (int y = 0; y < block[x].length; y++)
            {
                if (block[x][y])
                {
                    g.setColor(thisColor);
                    g.fillRect(posX + (x - offset) * 20, posY + (y - offset) * 20, 20, 20);
                    g.setColor(Color.BLACK);
                    g.drawRect(posX + (x - offset) * 20, posY + (y - offset) * 20, 20, 20);
                }
            }
        }
    }

    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        g.setFont(new Font("TimesRoman", Font.BOLD, 14));
        g.drawString(String.valueOf(score), 10, 180);
        g.drawString("Next Block", 10, 60);
        drawNext(g, 10, 70, nextBlock, color);
    }
}
