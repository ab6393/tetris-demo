
/**
 * CIS 120 HW10
 * (c) University of Pennsylvania
 *
 * @version 2.0, Mar 2013
 */
import java.awt.*;

/** A basic game object displayed as a black square, starting in the
 * upper left corner of the game court.
 *
 */
public class Tetrad
{

    public static final int SIZE = 20;
    public static final int INIT_X = 100;
    public static final int INIT_Y = 0;
    public static final int INIT_VEL_X = 0;
    public static final int INIT_VEL_Y = 20;
    public int pos_x = INIT_X;
    public int pos_y = INIT_Y;
    public int v_x = INIT_VEL_X;
    public int v_y = INIT_VEL_Y;
    public static BlockColor thisBlock;
    public static Color MYColor;
    public static boolean[][] Block;
    public static int offset;//used for position offset from this position
    public static CreateBlock tetradBlock;

    /**
     * Note that because we don't do anything special
     * when constructing a Tetrad, we simply use the
     * superclass constructor called with the correct parameters
     */
    public Tetrad(BlockColor color)
    {
        this(color, INIT_X, INIT_Y);
    }

    public Tetrad(BlockColor color, int posX, int posY)
    {
        this(color, posX, posY, INIT_VEL_X, INIT_VEL_Y);
    }

    public Tetrad(BlockColor color, int posX, int posY, int velX, int velY)
    {
        v_x = velX;
        v_y = velY;
        pos_x = posX;
        pos_y = posY;
        thisBlock = color;

        tetradBlock = new CreateBlock(color);
        MYColor = tetradBlock.getColor();
        Block = tetradBlock.getBlock();
        offset = Block.length / 2;

    }

    public void moveX(int counter)
    {
        if (counter == 0 || counter > 10)
        {
            pos_x += v_x;
        }

    }

    public void moveY()
    {
        pos_y += v_y;

    }

    public void draw(Graphics g)
    {
        for (int x = 0; x < Block.length; x++)
        {
            for (int y = 0; y < Block[x].length; y++)
            {
                if (Block[x][y])
                {
                    g.setColor(MYColor);
                    g.fillRect(pos_x + (x - offset) * SIZE, pos_y + (y - offset) * SIZE, SIZE, SIZE);
                    g.setColor(Color.BLACK);
                    g.drawRect(pos_x + (x - offset) * SIZE, pos_y + (y - offset) * SIZE, SIZE, SIZE);
                }
            }
        }
    }

    public boolean[][] getBlock()
    {
        return Block;
    }

    public void setBlock(boolean[][] block)
    {
        Block = block;
    }

    public int getOffset()
    {
        return offset;
    }
}
