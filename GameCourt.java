//Version 2.1.1
//implements slight fixes over v2.1

/**
 * CIS 120 HW10 (c) University of Pennsylvania
 *
 * @version 2.0, Mar 2013
 */
import java.awt.*;
import java.awt.event.*;
import java.util.Random;
import javax.swing.*;

/**
 * GameCourt
 *
 * This class holds the primary game logic of how different objects interact
 * with one another. Take time to understand how the timer interacts with the
 * different methods and how it repaints the GUI on every tick().
 *
 */
@SuppressWarnings("serial")
public class GameCourt extends JPanel
{

    // the state of the game logic
    public boolean playing = false;  // whether the game is running
    private JLabel status;       // Current status text (i.e. Running...)
    // Game constants
    public static final int COURT_WIDTH = 200;
    public static final int COURT_HEIGHT = 400;
    public static final int SQUARE_VELOCITY = 20;
    // Update interval for timer in milliseconds 
    public static final int INTERVAL = 35;
    public static final int DROP_SPEED = 500;
    //game related initialized classes or objects
    private Tetrad currentSquare; //creates the falling block
    public static Color[][] colorArray = new Color[10][20];
    public static BlockColor nextBlock = newBlock();//if the game resets before doing anything, this does not need to be initialized here
    public static BlockColor thisBlock = newBlock();//if the game resets before doing anything, this does not need to be initialized here
    public static NextBlockClone clone = new NextBlockClone();//passes data to clone so it can be read into SideBar
    //Variables used in various checks
    public static boolean[][] block;//for rotation and checks
    public static int offset;//offsets the blocks from the position of currentSquare. used to properly align the block array with the color array
    public static boolean[] clearLineTool;
    //control variables
    public static boolean leftpressed, uppressed, rightpressed, downpressed, spacePressed;
    public static int leftcounter;//used to delay the movement of the currentSquare so the user has better control
    public static int upcounter;//used to delay the movement of the currentSquare so the user has better control
    public static int rightcounter;//used to delay the movement of the currentSquare so the user has better control
    public static int spaceCounter = 0;//used to delay the movement of the currentSquare so the user has better control.
    public static boolean goingLeft;//direction control. manipulated by left/right presses 
    public static int counterPass;//passes whichever counter is active to the move method
    //scorevariables
    public static int downcounter;// used in score. multiply by constant and add to score modifier when not pressed (in tickdrop) or if downhit is true (in tick), then reset.
    public static int spacescorecounter;//add 1 to this in the spacepressed while loop, multiply by constant and add to score modifier when loop finsishes, then reset.
    public static int scoreModifier;//used for score. when a new block is created, add this to the score. it is modified whenever down or space is pressed
    public static long score = 0;//if implimented,pass to NextBlockClone whenever a new block is created
    private static int linesCleared;

    public GameCourt(JLabel status)
    {
        // creates border around the court area, JComponent method
        setBorder(BorderFactory.createLineBorder(Color.BLACK));

        clone.setAll(nextBlock, score);//if reset is called initially, omit

        // The timer is an object which triggers an action periodically
        // with the given INTERVAL. One registers an ActionListener with
        // this timer, whose actionPerformed() method will be called 
        // each time the timer triggers. We define a helper method
        // called tick() that actually does everything that should
        // be done in a single timestep.
        allNull(); //if reset is called initially, omit. sets all vars in color array to null
        Timer timer = new Timer(INTERVAL, new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                tick();
            }
        });

        Timer dropTimer = new Timer(DROP_SPEED, new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                tickDrop();
            }
        });
        timer.start();
        dropTimer.start();



        // Enable keyboard focus on the court area
        // When this component has the keyboard focus, key
        // events will be handled by its key listener.
        setFocusable(true);

        // this key listener allows the square to move as long
        // as an arrow key is pressed, by changing the square's
        // velocity accordingly. (The tick method below actually 
        // moves the square.)
        addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyPressed(KeyEvent e)
            {
                if (e.getKeyCode() == KeyEvent.VK_LEFT)
                {
                    currentSquare.v_x = -SQUARE_VELOCITY;
                    leftpressed = true;
                }
                else if (e.getKeyCode() == KeyEvent.VK_RIGHT)
                {
                    currentSquare.v_x = SQUARE_VELOCITY;
                    rightpressed = true;
                }
                else if (e.getKeyCode() == KeyEvent.VK_DOWN)
                {
                    downpressed = true;
                }
                else if (e.getKeyCode() == KeyEvent.VK_UP)
                {
                    uppressed = true;
                }
                else if (e.getKeyCode() == KeyEvent.VK_SPACE)
                {
                    spacePressed = true;
                }

            }

            @Override
            public void keyReleased(KeyEvent e)
            {
                if (e.getKeyCode() == KeyEvent.VK_LEFT)
                {
                    leftpressed = false;
                    if (!rightpressed)
                        currentSquare.v_x = 0;
                    else
                        currentSquare.v_x = SQUARE_VELOCITY;// takes care of people pressing both left and right then releasing one
                    leftcounter = 0;//resets delay
                }
                if (e.getKeyCode() == KeyEvent.VK_RIGHT)
                {
                    if (!leftpressed)
                    {
                        currentSquare.v_x = 0;
                    }
                    else
                    {
                        currentSquare.v_x = -SQUARE_VELOCITY;// takes care of people pressing both left and right then releasing one
                    }
                    rightpressed = false;
                    rightcounter = 0;//resets delay
                }
                if (e.getKeyCode() == KeyEvent.VK_DOWN)
                {
                    downpressed = false;
                }
                if (e.getKeyCode() == KeyEvent.VK_UP)
                {
                    uppressed = false;
                    upcounter = 0;//resets delay
                }
                if (e.getKeyCode() == KeyEvent.VK_SPACE)
                {
                    spacePressed = false;
                }
            }
        });

        this.status = status;
    }

    /**
     * (Re-)set the state of the game to its initial state.
     */
    public void reset()
    {
        thisBlock = newBlock();
        nextBlock = newBlock();
        score = 0;
        clone.setAll(nextBlock, score);

        currentSquare = new Tetrad(thisBlock);
        allNull();//may not need. i think you do

        playing = true;
        status.setText("Running...");

        // Make sure that this component has the keyboard focus
        requestFocusInWindow();
    }

    /**
     * This method is called every time the timer defined in the constructor
     * triggers.
     */
    void tick()
    {

        if (playing)
        {
            block = currentSquare.getBlock();
            offset = currentSquare.getOffset();
            //version 3.0 removes bad loss condition

            // the following take care of movement
            if (spacePressed)
            {
                if (spaceCounter == 0 || spaceCounter > 19)
                {
                    while (!downCheck(block, offset))
                    {
                        currentSquare.moveY();
                        //spacescorecounter++;
                    }
                    //score modifier += spacescorecounter * hard drop constant;
                    //spacescorecounter=0
                    downhit(block, offset);
                }
                spaceCounter++;
                if (spaceCounter > 19)
                {
                    spaceCounter = 0;//delay period is over, allow block to hard drop again
                }
            }
            else
                spaceCounter = 0;//resets the space counter
            if (uppressed)
            {
                if (upcounter == 0 || upcounter >= 10)
                {
                    rotateClockwise(block, offset);
                }
                upcounter++;
            }
            else
                upcounter = 0;//resets up counter


            if (rightpressed && leftpressed)//takes care of multipress idiots
            {
                if (currentSquare.v_x < 0)
                {
                    counterPass = leftcounter;//moving left, pass left counter
                    goingLeft = true;
                    rightcounter = 0;
                    leftcounter++;
                }
                else if (currentSquare.v_x > 0)//moving right, pass right counter
                {
                    counterPass = rightcounter;
                    leftcounter = 0;
                    goingLeft = false;
                    rightcounter++;
                }
            }
            else if (rightpressed)//moving right, pass right counter
            {
                leftcounter = 0;//may not be needed
                counterPass = rightcounter;
                goingLeft = false;
                rightcounter++;
            }
            else if (leftpressed) //moving left, pass left counter
            {
                rightcounter = 0;//probably not needed, but oh well
                counterPass = leftcounter;
                goingLeft = true;
                leftcounter++;
            }
            else //not moving, pass 0
            {
                rightcounter = 0;
                leftcounter = 0;
                counterPass = 0;
            }
            if (sideCheck(block, offset))//if at side or block to left
            {
                currentSquare.v_x = 0;
            }
            currentSquare.moveX(counterPass);//move
            if (downpressed)
            {
                //downcounter++;//used for score
                downhit(block, offset);
                currentSquare.moveY();
            }
            // check for the game end conditions

            /* if (currentSquare.pos_y == 0 && currentSquare.v_y == 0)
             * {
             * playing = false;
             * status.setText("you lose!");
             * }
             */

            clearLineTool = lineCheck();
            for (int y = 0; y < colorArray[0].length; y++)
            {
                if (clearLineTool[y])
                {
                    if (y != 0)
                    {
                        for (int z = y; z > 0; z--)
                        {
                            for (int x = 0; x < colorArray.length; x++)
                            {
                                colorArray[x][z] = colorArray[x][z - 1];
                            }
                        }
                    }
                    //initialize top line to null.
                    setLineToNull(0);
                }
            }
            // update the display
            repaint();
        }
    }

    void tickDrop()
    {
        if (playing)
        {
            if (!downpressed)
            {
                block = currentSquare.getBlock();
                offset = currentSquare.getOffset();
                downhit(block, offset);
                currentSquare.moveY();
            }
            repaint();
        }
    }

    @Override
    public void paintComponent(Graphics g)//draws a rectangle wherever there are non null color array values.
    {
        super.paintComponent(g);
        currentSquare.draw(g);

        for (int x = 0; x < colorArray.length; x++)
        {
            for (int y = 0; y < colorArray[x].length; y++)
            {
                if (colorArray[x][y] != null)
                {
                    g.setColor(colorArray[x][y]);
                    g.fillRect(x * 20, y * 20, 20, 20);
                    g.setColor(Color.BLACK);
                    g.drawRect(x * 20, y * 20, 20, 20);
                }
            }
        }
    }

    @Override
    public Dimension getPreferredSize()
    {
        return new Dimension(COURT_WIDTH, COURT_HEIGHT);
    }

    public void downhit(boolean[][] block, int offset)//if at the bottom or if a block below, and deals with creating a new block
    {
        if (downCheck(block, offset))//checks if at the bottom or if a block below
        {
            currentSquare.v_y = 0;//v2.1.1 fix: moved this out of for loop. slightly more efficient
            currentSquare.v_x = 0;//see above
            for (int z = 0; z < block.length; z++)
            {
                for (int y = 0; y < block[z].length; y++)
                {
                    if (block[z][y])
                    {
                        if (currentSquare.pos_y / 20 + y - offset >= 0)//added in v3.0 takes care of collision where y is still offscreen (aka during a loss)
                            colorArray[currentSquare.pos_x / 20 + z - offset][currentSquare.pos_y / 20 + y - offset] = currentSquare.MYColor;// should be fine. we'll find out
                    }
                }
            }
            thisBlock = nextBlock;
            nextBlock = newBlock();
            clone.setAll(nextBlock, score);
            //the following was changed in version 3.0
            currentSquare = (new Tetrad(thisBlock, 100, -40));//changed to -40, to take care of loss conditions. if it isn't colliding, it it moved to the original -20.
            if (downCheck(currentSquare.getBlock(), currentSquare.getOffset()))//loss check
            {
                playing = false;
                status.setText("you lose!");
            }
            else
            {
                currentSquare.pos_y += 20;//moved to original spawn position
                if (downCheck(currentSquare.getBlock(), currentSquare.getOffset()))//loss check
                {
                    playing = false;
                    status.setText("you lose!");
                }
            }
        }
    }

    public boolean downCheck(boolean[][] Block, int offset)
    {
        for (int z = 0; z < Block.length; z++)
        {
            for (int y = 0; y < Block[z].length; y++)
            {
                if (Block[z][y])
                {
                    int newX = currentSquare.pos_x / 20 + z - offset;
                    int newY = currentSquare.pos_y / 20 + y - offset + 1;
                    if (newX < 0)//handles out of bounds errors.
                    {
                        newX = 0;
                    }
                    if (newX > 9)//handles out of bounds errors. added in version 2.1.2
                    {
                        newX = 9;
                    }
                    if (newY < 0)//handles out of bounds errors. this one happens.
                    {
                        newY = 0;
                    }
                    if ((newY - 1 == 19 || colorArray[newX][newY] != null))
                    {
                        return true;
                    }

                }
            }
        }
        return false;
    }

    public boolean sideCheck(boolean[][] Block, int offset)
    {
        for (int z = 0; z < Block.length; z++)
        {
            for (int y = 0; y < Block[z].length; y++)
            {
                if (Block[z][y])
                {// errors
                    int blockCheckX = currentSquare.pos_x / 20 + z - offset;
                    int blockCheckY = currentSquare.pos_y / 20 + y - offset;
                    if (blockCheckY < 0)//block off top of screen. occurs when blocks just start falling
                        blockCheckY = 0;
                    if (blockCheckX <= 0)
                    {
                        if (goingLeft && currentSquare.v_y != 0 && currentSquare.v_x <= 0)//V2.1 removed uppressed, fixing multipress bug
                            return true;
                    }
                    else if (blockCheckX >= 9)
                    {
                        if (!goingLeft && currentSquare.v_y != 0 && currentSquare.v_x >= 0)//V2.1 removed uppressed, fixing multipress bug
                            return true;
                    }
                    else if (colorArray[blockCheckX + 1][blockCheckY] != null && !leftpressed && currentSquare.v_x >= 0)
                    {
                        return true;
                    }
                    else if (colorArray[blockCheckX - 1][blockCheckY] != null && !rightpressed && currentSquare.v_x <= 0)
                    {
                        return true;
                    }

                }
            }
        }
        return false;
    }

    public boolean blockOffScreen(boolean[][] Block, int offset)//only used for rotation, so a wall kick can occur
    {
        for (int z = 0; z < Block.length; z++)
        {
            for (int y = 0; y < Block[z].length; y++)
            {
                if (Block[z][y])
                {// errors
                    if (currentSquare.pos_x / 20 + z - offset < 0 || currentSquare.pos_x / 20 + z - offset > 9 || currentSquare.pos_y / 20 + y - offset < 0 || currentSquare.pos_y / 20 + y - offset > 19)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void rotateClockwise(boolean[][] current, int offset)
    {
        boolean[][] rotated = new boolean[current.length][current[0].length];
        for (int z = 0; z < current.length; z++)
        {
            for (int y = 0; y < current[z].length; y++)//z = 0, y=0 new should be x=2,y=0 z=1, y=0 should be x=2 y=1 z=2 y=0 should be z=2 y=2
            {
                rotated[current[z].length - y - 1][z] = current[z][y];
            }
        }
        if (!canRotate(rotated, offset))//V2.1.0 changed from sidecheck for more precision, and helped rid myself of a multipress bug
        {
            if (blockOffScreen(rotated, offset))//if cannot rotate b/c the block will be offscreen, try kicking
            {
                if (wallKick(rotated, offset))//if it can kick, moves the block the kick distance and returns true. otherwise, returns false, and does not kick
                    currentSquare.setBlock(rotated);//if it
            }
        }
        else
            currentSquare.setBlock(rotated);//V2.1 made this an else so it won't kick regardless of changes I mage
    }

    public boolean canRotate(boolean[][] current, int offset)
    {
        int currentPosX;
        int currentPosY;
        for (int z = 0; z < current.length; z++)
        {
            for (int y = 0; y < current[z].length; y++)
            {
                if (current[z][y])
                {// errors
                    currentPosX = currentSquare.pos_x / 20 + z - offset;
                    currentPosY = currentSquare.pos_y / 20 + y - offset;
                    if (currentPosY < 0)
                        currentPosY = 0;
                    else if (currentPosY > 19)//if at bottom
                    {
                        return false;
                    }
                    if (currentPosX < 0) // or at sides
                    {
                        return false;
                    }
                    else if (currentPosX > 9)
                    {
                        return false;
                    }
                    else if (colorArray[currentPosX][currentPosY] != null) //or anywhere in the new potential rotation
                    {
                        return false;
                    }
                    //V2.1.1 removed duplicate statement
                }
            }
        }
        return true;
    }

    public static boolean[] lineCheck()//creates an array of bools. returns true if the line is full. corresponds to the y value of the color array
    {
        boolean[] fullLines = new boolean[20];
        for (int y = 0; y < colorArray[0].length; y++)
        {
            fullLines[y] = thisLineFull(y);
        }
        return fullLines;
    }

    public static boolean thisLineFull(int y)//checks if a line is full. sets the corresponding boolean value in the array
    {
        for (int z = 0; z < colorArray.length; z++)
        {
            if (colorArray[z][y] == null)
                return false;
        }
        return true;
    }

    public static void setLineToNull(int y)//clears an entire row
    {
        for (int x = 0; x < colorArray.length; x++)
        {
            colorArray[x][y] = null;
        }
    }

    public static void allNull()//clears entire map
    {
        for (int y = 0; y < colorArray[0].length; y++)
        {
            setLineToNull(y);
        }
    }
//fix in V2.1.1 removed inciment method. it was used for debugging and is pointless

    public boolean wallKick(boolean[][] kick, int offset)//wall kicks occur in most versions of tetris (not, however, the flash version :( ). occurs if block is against the wall but can otherwise rotate
    {//moves the block away from the wall, then rotates, unless other blocks prevent it from doing so
        int offsetDirection;//left =1, right =-1
        boolean canKick = true;//V2.1.1 removed empty statement (i had one to many ;s)
        if (currentSquare.pos_x <= 20 * offset)
        {
            offsetDirection = 1;
        }
        else
            offsetDirection = -1;
        for (int z = 0; z < kick.length; z++)
        {
            for (int y = 0; y < kick[z].length; y++)
            {
                if (currentSquare.pos_y / 20 + y - offset > 19)//if kicking would put it off the bottom, don't. it shouldn't happen, but better safe than with an error
                {
                    canKick = false;
                }
                else if (currentSquare.pos_y / 20 + y - offset > -1 && currentSquare.pos_x / 20 + z - offset + offsetDirection > -1 && colorArray[currentSquare.pos_x / 20 + z - offset + offsetDirection][currentSquare.pos_y / 20 + y - offset] != null)
                {
                    canKick = false;
                }
            }
        }

        if (canKick)//if can kick, then kick it
        {
            if (kick.length == 4 && kick[2][0] && kick[2][3])
                currentSquare.pos_x += 20 * (offset - 1) * offsetDirection;
            else
                currentSquare.pos_x += 20 * offset * offsetDirection;
        }
        return canKick;
    }

    public static BlockColor newBlock() //creates new random block
    {
        Random newerBlock = new Random();
        int nextCheck = newerBlock.nextInt(100);
        if (nextCheck < 14)
        {
            return BlockColor.RED;
        }
        else if (nextCheck < 28)
        {
            return BlockColor.ORANGE;
        }
        else if (nextCheck < 42)
        {
            return BlockColor.YELLOW;
        }
        else if (nextCheck < 57)
        {
            return BlockColor.GREEN;
        }
        else if (nextCheck < 71)
        {
            return BlockColor.BLUE;
        }
        else if (nextCheck < 86)
        {
            return BlockColor.CYAN;
        }
        else
            return BlockColor.PURPLE;
    }
}
