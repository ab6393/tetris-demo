
import java.awt.Color;

//Author: Andrew Baumher
public class CreateBlock
{
        private boolean[][] Block;
        private Color color;
    public CreateBlock(BlockColor thisBlock)
    {
        switch (thisBlock)
        {
            case RED:
                Block = new boolean[4][4];
                Block[1][1] = true;
                Block[1][2] = true;
                Block[2][1] = true;
                Block[2][2] = true;
                color = Color.RED;
                break;
            case ORANGE:
                Block = new boolean[4][4];
                Block[0][1] = true;
                Block[1][1] = true;
                Block[2][1] = true;
                Block[3][1] = true;
                color = Color.ORANGE;
                break;
            case YELLOW:
                Block = new boolean[3][3];
                Block[1][1] = true;
                Block[1][0] = true;
                Block[0][1] = true;
                Block[2][1] = true;
                color = Color.YELLOW;
                break;
            case GREEN:
                Block = new boolean[3][3];
                Block[0][1] = true;
                Block[1][1] = true;
                Block[1][0] = true;
                Block[2][0] = true;
                color = Color.GREEN;
                break;
            case BLUE:
                Block = new boolean[3][3];
                Block[0][0] = true;
                Block[0][1] = true;
                Block[1][1] = true;
                Block[2][1] = true;
                color = Color.BLUE;
                break;
            case CYAN:
                Block = new boolean[3][3];
                Block[2][1] = true;
                Block[1][1] = true;
                Block[1][0] = true;
                Block[0][0] = true;
                color = Color.CYAN;
                break;
            case PURPLE:
                Block = new boolean[3][3];
                Block[0][1] = true;
                Block[1][1] = true;
                Block[2][1] = true;
                Block[2][0] = true;
                color = new Color(139, 0, 204);
                break;
        }


    }

    public boolean[][] getBlock()
    {
        return Block;
    }
    public Color getColor()
    {
        return color;
    }
}
