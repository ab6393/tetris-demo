//Author: Andrew Baumher
import java.awt.Color;

public class NextBlockClone
{

    private static Color color;
    private static boolean[][] booleanArray;
    private static CreateBlock me;
    private static long score;

    public NextBlockClone()
    { 
    }

    public static Color getColor()
    {
        return color;
    }
    public static boolean[][] getBlock()
    {
        return booleanArray;
    }
    public static long getScore()
    {
        return score;
    }

    public static void setAll(BlockColor blockColor, long Score)
    {
        me = new CreateBlock(blockColor);
        color = me.getColor();
        booleanArray = me.getBlock();
        score = Score;
    }
}
